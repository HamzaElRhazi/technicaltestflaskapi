#Développer une API Type CRUD enregistrant les données dans une base de donnée: enregistrement de prénom et âge

from flask import Flask, request
from flask_restful import Resource, Api
from models import db, PersonneModel


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///personnesData.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

api = Api(app)
db.init_app(app)

@app.before_first_request
def create_table():
    db.create_all()

#Données fictifs des personnes


personnes_dump = {
    1:["Ghislain",38],
    2:["Thierry", 61],
    3:["Didier", 52]
}


class viewPersonnes(Resource):
    def post(self):
        #inserer les elements du dictionnaire dans la table personnes
        prenom1 = personnes_dump[1][0]
        age1 =  personnes_dump[1][1]
        ghislain = PersonneModel(prenom1, age1)

        prenom2 = personnes_dump[2][0]
        age2 = personnes_dump[2][1]
        thierry = PersonneModel(prenom2, age2)

        prenom3 = personnes_dump[3][0]
        age3 = personnes_dump[3][1]
        didier = PersonneModel(prenom3, age3)

        #ajout dans la bases de données
        db.session.add(ghislain)
        db.session.add(thierry)
        db.session.add(didier)
        db.session.commit()

        return [ghislain.json(), thierry.json(), didier.json()]

api.add_resource(viewPersonnes,'/personnes', '/')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')


