from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class PersonneModel(db.Model):
    __tablename__ = 'personnes'

    id = db.Column(db.Integer, primary_key=True)
    prenom = db.Column(db.String(80))
    age = db.Column(db.Integer())


    def __init__(self, prenom,age):
        self.prenom = prenom
        self.age = age

    def __repr__(self):
        return f"{self.prenom}:{self.age}"


    def json(self):
        return {"prenom": self.prenom, "age": self.age}
