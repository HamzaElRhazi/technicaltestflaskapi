from flask import Flask, render_template, request, redirect, jsonify
from models import db, PersonneModel

app=Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///personnesData.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db.init_app(app)


@app.before_first_request
def create_table():
    db.create_all()

@app.route('/Hello')
def hello():
    return f"<h1>Hello</h1>"

@app.route('/distribution_age')
def RetrieveList():
    personnes = PersonneModel.query.all()
    return render_template('personnes_list.html',personnes = personnes)

app.run(debug=False, host='localhost', port=5050)