**<h1>Procédure d'installation:</h1>**

**Le projet contient deux couches, une API REST backend, packagée dans sa propre image, et une couche Front packagée dans une image docker séparrée** 

<h2>localisation des images docker back et front</h2>
==========================================
les images sont disponibles dans le Docker Hub dans le [repo] (https://hub.docker.com/repository/docker/hamzaelrhazi/bnp_technical_test_repo)

Pour lancer le programme, il faut récuperer les images docker avec un pull, et les lancer avec :
``
docker container run -d --name api_rest -p 5000:5000 hamzaelrhazi/bnp_technical_test_repo
``

<h2>Fonctionnalité de l'APP :</h2>
===============================================
Pour enregistrer les données fictifs dans la base de données : utiliser la commande curl
``
curl http://localhost:5000/personnes -X POST
`` 

Pour visualiser la distribution des ages, utiliser le chemin ``http://localhost:5050/distribution_age``